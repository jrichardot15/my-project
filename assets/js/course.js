//window.location.search returns the query string part of the URL
//console.log(window.location.search)

//instantiate a URLSearchParams object so we can execute methods to access parts of the query sting
let params = new URLSearchParams(window.location.search)

let courseId = params.get('courseId')
let userId = localStorage.getItem("id");

//retrieve the JWT stored in our local storage
let token = localStorage.getItem("token");

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");
 
fetch(`http://localhost:3000/api/courses/${courseId}`)
.then((res) => res.json())
.then((data) => {
	console.log(data)
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price
	enrollContainer.innerHTML = '<button id="enrollButton" class="btn btn-success">Enroll</button>'

	let enrollButton = document.querySelector("#enrollButton")

	enrollButton.addEventListener("click", () => {
		//enroll the user for the course
		fetch('http://localhost:3000/api/users/enroll', {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId,
				userId: userId
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data === true){
				//enrollment is successful
				alert("Thank you for enrolling! See you!")
				window.location.replace('./courses.html')
			}else{
				alert("Enrollment failed")
			}
		})
	})
})
