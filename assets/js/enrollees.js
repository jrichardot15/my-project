//window.location.search returns the query string part of the URL
//console.log(window.location.search)

//instantiate a URLSearchParams object so we can execute methods to access parts of the query sting
let params = new URLSearchParams(window.location.search)

let courseId = params.get('courseId')
let userId = localStorage.getItem("id");

//retrieve the JWT stored in our local storage
let token = localStorage.getItem("token");

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");
 
fetch(`http://localhost:3000/api/courses/${courseId}`)
.then((res) => res.json())
.then((data) => {
	// console.log(data)
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price
	// enrolleesList.innerHTML = 

// let token = localStorage.getItem("token");

let userIds = []
let userNames = []

fetch(`http://localhost:3000/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	// console.log(data)
	for(let i = 0; i < data.enrollees.length; i++){
		userIds.push(data.enrollees[i].userId)
	}

	fetch('http://localhost:3000/api/users/admin', {
		method: 'GET',
		headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
	})
	.then(res => res.json())
	.then(data => {
		// console.log(data)
		for(let i = 0; i < data.length; i++){

			if(userIds.includes(data[i]._id)){
				userNames.push(data[i].firstName + " " + data[i].lastName)
			}

	}
		// userNames = data.firstName + " " + data.lastName

	let userList = userNames.join(", ")
  //Course names are here in coursesList
  console.log(userList)
	enrolleesList.innerHTML = `<h5>List of Enrollees: </h5> ${userList}`

})
})
})