let params = new URLSearchParams(window.location.search)

let userId = params.get('user')
let token = localStorage.getItem("token");
let courseId = params.get('courseId')
let userName = document.querySelector("#userName");
let email = document.querySelector("#email");
let mobileNo = document.querySelector("#mobileNo");
let courses = document.querySelector("#courses");
let userProfile = document.querySelector("#profileContainer");
let coursesEnrolled = document.querySelector("#coursesEnrolled")
 
fetch(`http://localhost:3000/api/users/details`, {
	method:'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
})
.then((res) => res.json())
.then((data) => {
 // console.log(data.enrollments)
		userName.innerHTML = data.firstName + " " + data.lastName
		email.innerHTML = data.email
		mobileNo.innerHTML = data.mobileNo

let courseIds = []
let courseNames = []

fetch('http://localhost:3000/api/users/details', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {

	for(let i = 0; i < data.enrollments.length; i++){
		courseIds.push(data.enrollments[i].courseId)
	}

	fetch('http://localhost:3000/api/courses')
	.then(res => res.json())
	.then(data => {

		for(let i = 0; i < data.length; i++){

			if(courseIds.includes(data[i]._id)){
				courseNames.push(data[i].name)
			}
	}
	let coursesList = courseNames.join(", ")
  //Course names are here in coursesList
  console.log(coursesList)

		coursesEnrolled.innerHTML = `${coursesList}`

})

})
})

//fetch the courses from our API
// fetch('http://localhost:3000/api/users/details', {
// 		method:'GET',
// 	headers: {
// 		'Content-Type': 'application/json',
// 		'Authorization': `Bearer ${token}`
// 	}
// })
// .then(res => res.json())
// .then(data => {
// 	// console.log(data)
// 	let enrollmentsData;

// // 	//if the number of courses fetched is less than 1, display no courses available.
// 	if(data.length < 1){
// 		courseData = "No courses available."
// 	}else{
// // 		//else iterate the courses collection and display each course
// 		enrollmentsData = data.enrollments.map(enrollments => {
// 			console.log(data)
			
// 			return (
// 					`${enrollments.name}`
// 					)

// 		}).join("")
// 	}

// 	let container = document.querySelector('#coursesContainer')
// 	container.innerHTML = enrollmentsData
// })
// 	}
// })